WAV 2 PNG
=========

Copies WAV PCM data from a 24-bit mono audio file of any sample rate directly in to a PNG file as 24-bit RGB image data.  The image width is fixed at 1920 and the height will correspond to the WAV length.

Depends on libpng.
