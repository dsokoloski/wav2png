#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <math.h>

#include <sys/types.h>

#define PNG_DEBUG 3
#include <png.h>

#define PNG_WIDTH 1920
#define PNG_BIT_DEPTH 8

enum WAV_CHUNK {
    RIFF_HEADER = 0x46464952,
    WAV_RIFF = 0x54651475,
    FORMAT = 0x020746d66,
    TEXT = 0x478747c6,
    INST = 0x478747c6,
    SAMPLE = 0x6c706d73,
    FACT = 0x47361666,
    DATA = 0x61746164,
    JUNK = 0x4b4e554a,
};

enum WAV_FORMAT {
    PCM = 0x01,
    IEEE_FP = 0x03,
    ALAW = 0x06,
    MULAW = 0x07,
    IMAADPCM = 0x11,
    ITUG723ADPCM = 0x16,
    GSM610 = 0x31,
    ITUG721ADPCM = 0x40,
    MPEG = 0x50,
    EXT = 0xfffe
};

struct wav_header_t {
    uint32_t chunk_id;
    uint32_t data_size;
    uint32_t format_size;
    uint16_t format;
    uint16_t channels;
    unsigned channel_count;
    uint32_t sample_rate;
    uint32_t bits_per_second;
    uint16_t format_block_align;
    uint16_t bit_depth;
    uint32_t extra_data;
    uint32_t header_id;
    uint32_t mem_size;
    uint32_t riff_style;
    uint32_t skip_size;
};

struct png_state_t {
    int x, y;

    int width, height;
    png_byte color_type;
    png_byte bit_depth;

    png_structp png_ptr;
    png_infop info_ptr;
    int number_of_passes;
    size_t stride;
    png_bytep row;
};

static long read_wav_header(FILE *fh, struct wav_header_t *hdr)
{
    bool data_chunk = false;

    while (! data_chunk) {
        assert(fread(&hdr->chunk_id, sizeof(uint32_t), 1, fh) == 1);

        switch (hdr->chunk_id) {
        case FORMAT:
            assert(fread(&hdr->format_size, sizeof(uint32_t), 1, fh) == 1);
            fprintf(stdout, "format_size: %08x\n", hdr->format_size);
            assert(fread(&hdr->format, sizeof(uint16_t), 1, fh) == 1);
            fprintf(stdout, "format: %04x\n", hdr->format);
            assert(hdr->format == PCM);
            assert(fread(&hdr->channels, sizeof(uint16_t), 1, fh) == 1);
            fprintf(stdout, "channels: %04x\n", hdr->channels);
            hdr->channel_count = (int)hdr->channels;
            assert(hdr->channel_count == 1);
            assert(fread(&hdr->sample_rate, sizeof(uint32_t), 1, fh) == 1);
            fprintf(stdout, "sample_rate: %08x\n", hdr->sample_rate);
            assert(fread(&hdr->bits_per_second, sizeof(uint32_t), 1, fh) == 1);
            fprintf(stdout, "bits_per_second: %08x\n", hdr->bits_per_second);
            assert(fread(&hdr->format_block_align, sizeof(uint16_t), 1, fh) == 1);
            fprintf(stdout, "format_block_align: %04x\n", hdr->format_block_align);
            assert(fread(&hdr->bit_depth, sizeof(uint16_t), 1, fh) == 1);
            fprintf(stdout, "bit_depth: %04x\n", hdr->bit_depth);
            assert(hdr->bit_depth == 24);

            if (hdr->format_size == 18) {
                assert(fread(&hdr->extra_data, sizeof(uint32_t), 1, fh) == 1);
                fprintf(stdout, "extra_data: %08x\n", hdr->extra_data);
                fseek(fh, hdr->extra_data, SEEK_CUR);
            }
            break;

        case RIFF_HEADER:
            hdr->header_id = hdr->chunk_id;
            assert(fread(&hdr->mem_size, sizeof(uint32_t), 1, fh) == 1);
            fprintf(stdout, "mem_size: %08x\n", hdr->mem_size);
            assert(fread(&hdr->riff_style, sizeof(uint32_t), 1, fh) == 1);
            fprintf(stdout, "riff_style: %08x\n", hdr->riff_style);
            break;

        case DATA:
            data_chunk = true;
            assert(fread(&hdr->data_size, sizeof(uint32_t), 1, fh) == 1);
            fprintf(stdout, "data_size: %08x\n", hdr->data_size);
            break;

        default:
            assert(fread(&hdr->skip_size, sizeof(uint32_t), 1, fh) == 1);
            fprintf(stdout, "skip_size: %08x\n", hdr->skip_size);
            fseek(fh, hdr->skip_size, SEEK_CUR);
            break;
        }
    }

    return ftell(fh);
}

static void write_png(
    struct wav_header_t *wav_header, FILE *fh_wav,
    struct png_state_t *png_state, FILE *fh_png)
{
    
    png_state->png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    assert(png_state->png_ptr);

    png_state->info_ptr = png_create_info_struct(png_state->png_ptr);
    assert(png_state->info_ptr);

    assert(!setjmp(png_jmpbuf(png_state->png_ptr)));

    png_init_io(png_state->png_ptr, fh_png);

    assert(!setjmp(png_jmpbuf(png_state->png_ptr)));

    png_set_IHDR(
        png_state->png_ptr, png_state->info_ptr,
        png_state->width, png_state->height,
        png_state->bit_depth, png_state->color_type,
        PNG_INTERLACE_NONE,
        PNG_COMPRESSION_TYPE_BASE,
        PNG_FILTER_TYPE_BASE
    );

    png_write_info(png_state->png_ptr, png_state->info_ptr);

    for (png_state->y = 0; png_state->y < png_state->height; png_state->y++) {
        memset(png_state->row, 0, png_state->stride);
        fread(png_state->row, png_state->stride, 1, fh_wav);
        png_write_row(png_state->png_ptr, png_state->row);
    }
#if 0
    assert(!setjmp(png_jmpbuf(png_state->png_ptr)));

    png_write_image(png_state->png_ptr, png_state->row_pointers);
#endif
    assert(!setjmp(png_jmpbuf(png_state->png_ptr)));

    png_write_end(png_state->png_ptr, NULL);
}

int main(int argc, char *argv[])
{
    struct wav_header_t wav_header;
    struct png_state_t png_state;
    FILE *fh_wav = NULL, *fh_png = NULL;

    if (argc != 3) {
        fprintf(stderr, "Usage: %s <wav> <png>\n", argv[0]);
        return 1;
    }

    fh_wav = fopen(argv[1], "rb");
    if (fh_wav == NULL) {
        fprintf(stderr, "Error opening WAV file: %s\n", argv[1]);
        return 1;
    }

    memset(&wav_header, 0, sizeof(struct wav_header_t));
    read_wav_header(fh_wav, &wav_header);

    fh_png = fopen(argv[2], "wb");
    if (fh_png == NULL) {
        fprintf(stderr, "Error opening PNG file: %s\n", argv[2]);
        return 1;
    }

    memset(&png_state, 0, sizeof(struct png_state_t));
    png_state.width = PNG_WIDTH;
    png_state.height = (int)ceilf((float)wav_header.data_size / (float)wav_header.format_block_align / (float)PNG_WIDTH);
    png_state.bit_depth = PNG_BIT_DEPTH;
    png_state.color_type = PNG_COLOR_TYPE_RGB;
    png_state.stride = 3 * png_state.width * sizeof(png_byte);
    png_state.row = malloc(png_state.stride);
    fprintf(stdout, "png width x height: %d x %d\n", png_state.width, png_state.height);
    write_png(&wav_header, fh_wav, &png_state, fh_png);

    return 0;
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
